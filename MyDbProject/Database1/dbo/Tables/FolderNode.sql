﻿CREATE TABLE [dbo].[FolderNode] (
    [Id]               INT            IDENTITY (0, 1) NOT NULL,
    [FolderNodeName]   NVARCHAR (500) NOT NULL,
    [FileSystemNodeId] INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

