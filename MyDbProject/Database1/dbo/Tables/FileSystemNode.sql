﻿CREATE TABLE [dbo].[FileSystemNode] (
    [Id]             INT            IDENTITY (0, 1) NOT NULL,
    [ParentNodeId]   INT            NULL,
    [ParentNodePath] NVARCHAR (MAX) NULL,
    [NodeType]       INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

