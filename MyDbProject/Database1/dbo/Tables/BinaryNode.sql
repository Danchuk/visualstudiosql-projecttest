﻿CREATE TABLE [dbo].[BinaryNode] (
    [Id]               INT            IDENTITY (0, 1) NOT NULL,
    [BinaryNodeName]   NVARCHAR (500) NOT NULL,
    [FileSystemNodeId] INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

