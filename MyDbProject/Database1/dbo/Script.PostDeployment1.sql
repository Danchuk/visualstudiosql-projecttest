﻿/*
Post-Deployment Script Template                            
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.        
 Use SQLCMD syntax to include a file in the post-deployment script.            
 Example:      :r .\myfile.sql                                
 Use SQLCMD syntax to reference a variable in the post-deployment script.        
 Example:      :setvar TableName MyTable                            
               SELECT * FROM [$(TableName)]                    
--------------------------------------------------------------------------------------
*/

EXEC dbo.CreateRootNode;
EXEC dbo.AddFileSystemNode '/', 'Creating Digital Images', 1;
EXEC dbo.AddFileSystemNode '/Creating Digital Images/', 'Resources', 1;
EXEC dbo.AddFileSystemNode '/Creating Digital Images/', 'Evidence', 1;
EXEC dbo.AddFileSystemNode '/Creating Digital Images/', 'Graphic Products', 1;
EXEC dbo.AddFileSystemNode '/Creating Digital Images/Resources/', 'Primary Sources', 1;
EXEC dbo.AddFileSystemNode '/Creating Digital Images/Resources/', 'Secondary Sources', 1;
EXEC dbo.AddFileSystemNode '/Creating Digital Images/Graphic Products/', 'Process', 1;
EXEC dbo.AddFileSystemNode '/Creating Digital Images/Graphic Products/', 'Final Product', 1;


EXEC dbo.AddFileSystemNode '/Creating Digital Images/', 'test', 1;
EXEC dbo.AddFileSystemNode '/Creating Digital Images/test/', 'SomeFolder', 1;
EXEC dbo.AddFileSystemNode '/Creating Digital Images/test/', 'bin.txt', 2;
EXEC dbo.AddFileSystemNode '/', 'someFolderToRoot', 1;
EXEC dbo.RemoveFileSystemNode '/Creating Digital Images/test/', 'SomeFolder', 1;
