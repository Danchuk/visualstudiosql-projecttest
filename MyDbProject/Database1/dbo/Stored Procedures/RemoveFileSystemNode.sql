﻿CREATE PROCEDURE [dbo].[RemoveFileSystemNode]
    @parentNodePath NVARCHAR (MAX),
    @nodeName       NVARCHAR (500),
    @nodeType       int
AS
    DECLARE @fileSystemNodeId      INT
    DECLARE @result                INT
    DECLARE @countFolderNode       INT
    DECLARE @countBinaryNode       INT

    SET @result = -1;

    BEGIN TRANSACTION
    BEGIN TRY
        IF (@nodeType = 1)
            EXEC @result = dbo.GetFolderNodeId @parentNodePath, @nodeName;
        ELSE IF (@nodeType = 2)
            EXEC @result = dbo.GetBinaryNodeId @parentNodePath, @nodeName;

        IF (@result >= 0)
            BEGIN
                EXEC @fileSystemNodeId = dbo.GetFileSystemNodeId @parentNodePath, @nodeName, @nodeType;
                                     
                IF (@nodeType = 1)
                    BEGIN
                        DELETE  folderNodeTable 
                        FROM dbo.FolderNode AS folderNodeTable 
                        LEFT JOIN dbo.FileSystemNode ON dbo.FileSystemNode.Id = folderNodeTable.FileSystemNodeId
                        WHERE dbo.FileSystemNode.ParentNodePath = @parentNodePath AND folderNodeTable.FolderNodeName = @nodeName
                    END
                ELSE IF (@nodeType = 2)
                    BEGIN
                        DELETE  binaryNodeTable 
                        FROM dbo.BinaryNode AS binaryNodeTable 
                        LEFT JOIN dbo.FileSystemNode ON dbo.FileSystemNode.Id = binaryNodeTable.FileSystemNodeId
                        WHERE dbo.FileSystemNode.ParentNodePath = @parentNodePath AND binaryNodeTable.BinaryNodeName = @nodeName
                    END

                EXEC @countFolderNode = dbo.GetFolderNodeCount @fileSystemNodeId;
                If (@countFolderNode = 0)
                    EXEC @fileSystemNodeId = dbo.UpdateFileSystemNodeType @fileSystemNodeId, 1, 'RemoveBit';

                EXEC @countBinaryNode = dbo.GetBinaryNodeCount @fileSystemNodeId;
                If (@countBinaryNode = 0)
                    EXEC @fileSystemNodeId = dbo.UpdateFileSystemNodeType @fileSystemNodeId, 2, 'RemoveBit';

                IF (@countFolderNode = 0 AND @countBinaryNode = 0)
                    BEGIN
                        DELETE  fileSystemNodeTable 
                        FROM dbo.FileSystemNode AS fileSystemNodeTable 
                        WHERE fileSystemNodeTable.Id = @fileSystemNodeId
                    END

                COMMIT TRANSACTION;
            END
        ELSE
            BEGIN
                ROLLBACK TRANSACTION;
                RAISERROR('The node is not exist, so there is not node for removing.', 10, 8);
            END
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;
        THROW;
    END CATCH

RETURN @result
