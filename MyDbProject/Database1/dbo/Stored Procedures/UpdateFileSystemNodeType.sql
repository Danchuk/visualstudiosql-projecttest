﻿CREATE PROCEDURE [dbo].[UpdateFileSystemNodeType]
    @fileSystemNodeId int,
    @nodeType         int,
    @operationType    NVARCHAR (10)
AS
    DECLARE @currentNodeType  INT

    BEGIN TRANSACTION
    BEGIN TRY
        SET @currentNodeType = (SELECT dbo.FileSystemNode.NodeType FROM dbo.FileSystemNode WHERE dbo.FileSystemNode.Id = @fileSystemNodeId);

        IF (@currentNodeType IS NULL)
            RAISERROR('Can not obtain the Id form the FileSystemNode table.', 11, 6);

        IF (@operationType = 'AddBit')
            SET @currentNodeType = @currentNodeType | @nodeType;
        ELSE IF (@operationType = 'RemoveBit')
            SET @currentNodeType = @currentNodeType & ~@nodeType;
        ELSE
            RAISERROR('The operationType parametr is incorrect.', 11, 6);

        UPDATE dbo.FileSystemNode   
        SET dbo.FileSystemNode.NodeType = @currentNodeType  
        WHERE dbo.FileSystemNode.Id = @fileSystemNodeId;

        COMMIT TRANSACTION; 
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION; 
        THROW;
    END CATCH

RETURN @fileSystemNodeId
