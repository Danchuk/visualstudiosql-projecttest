﻿CREATE PROCEDURE [dbo].[AddFileSystemNode]
    @parentNodePath NVARCHAR (MAX),
    @nodeName       NVARCHAR (500),
    @nodeType       int
AS
    DECLARE @parentNodeId      INT
    DECLARE @fileSystemNodeId  INT
    DECLARE @result            INT
    DECLARE @addFSNodeError    INT
    DECLARE @addFSNodeErrState INT

    SET @result = -1;
    SET @addFSNodeErrState = 1;

    BEGIN TRANSACTION
    BEGIN TRY

        IF (@nodeType = 1)
            EXEC @result = dbo.GetFolderNodeId @parentNodePath, @nodeName;
        ELSE IF (@nodeType = 2)
            EXEC @result = dbo.GetBinaryNodeId @parentNodePath, @nodeName;
    
        IF (@result < 0)
            BEGIN
                EXEC @parentNodeId = dbo.GetParentNodeId @parentNodePath;

                IF (@parentNodeId < 0)
                    RAISERROR('Can not obtain the parent node id.', 11, @addFSNodeErrState);

                SET @fileSystemNodeId = (SELECT dbo.FileSystemNode.Id FROM dbo.FileSystemNode WHERE dbo.FileSystemNode.ParentNodePath = @parentNodePath);
                SET @addFSNodeErrState = 2;

                IF (@fileSystemNodeId IS NULL)
                    BEGIN
                        INSERT INTO dbo.FileSystemNode (ParentNodeId, ParentNodePath, NodeType) VALUES ( @parentNodeId, @parentNodePath, @nodeType);
                        SET @addFSNodeError = @@ERROR;
                        SET @addFSNodeErrState = 3;
                        SET @fileSystemNodeId = ident_current('FileSystemNode');
                    END
                ELSE
                    EXEC @fileSystemNodeId = dbo.UpdateFileSystemNodeType @fileSystemNodeId, @nodeType, 'AddBit';

                IF (@addFSNodeError <> 0)
                    RAISERROR('Can not update the data in the FileSystemNode table.', 11, @addFSNodeErrState);

                IF (@nodeType = 1)
                    BEGIN
                        INSERT INTO dbo.FolderNode (FileSystemNodeId, FolderNodeName) VALUES ( @fileSystemNodeId, @nodeName);
                        SET @addFSNodeError = @@ERROR;
                        SET @addFSNodeErrState = 4;
                        SET @result = ident_current('FolderNode');
                    END
                ELSE IF (@nodeType = 2)
                    BEGIN
                        INSERT INTO dbo.BinaryNode(FileSystemNodeId, BinaryNodeName) VALUES ( @fileSystemNodeId, @nodeName);
                        SET @addFSNodeError = @@ERROR;
                        SET @addFSNodeErrState = 5;
                        SET @result = ident_current('BinaryNode');
                    END;

                IF (@addFSNodeError <> 0)
                    RAISERROR('Can not insert the data to the FolderNode or BinaryNode.', 11, @addFSNodeErrState);

                COMMIT TRANSACTION;
            END
        ELSE
            BEGIN
                ROLLBACK TRANSACTION;
                RAISERROR('The node of FileSystemNode table is exist now, so procedure completed without creating a duplicate.', 10, 1);
            END
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;
        THROW;
    END CATCH

RETURN @result;