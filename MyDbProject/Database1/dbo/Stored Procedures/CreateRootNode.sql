﻿CREATE PROCEDURE [dbo].[CreateRootNode]
AS
    DECLARE @fileSystemNodeId      INT

    BEGIN TRANSACTION
    BEGIN TRY
        SET @fileSystemNodeId = -1;

        SET @fileSystemNodeId = (SELECT dbo.FileSystemNode.Id FROM dbo.FileSystemNode WHERE dbo.FileSystemNode.ParentNodePath IS NULL AND dbo.FileSystemNode.ParentNodeId IS NULL);
        IF (@fileSystemNodeId IS NULL)
            BEGIN
                INSERT INTO dbo.FileSystemNode (ParentNodeId, ParentNodePath, NodeType) VALUES ( NULL, NULL, 1);
    
                SET @fileSystemNodeId = ident_current('FileSystemNode');

                INSERT INTO dbo.FolderNode (FileSystemNodeId, FolderNodeName) VALUES ( @fileSystemNodeId, '');

                INSERT INTO dbo.FileSystemNode (ParentNodeId, ParentNodePath, NodeType) VALUES ( @fileSystemNodeId, '/', 1);

                SET @fileSystemNodeId = ident_current('FileSystemNode');

                COMMIT TRANSACTION;
            END
        ELSE
            BEGIN
                ROLLBACK TRANSACTION;
                RAISERROR('The root node is exist now, so procedure completed without creating the root node duplicate.', 10, 7);
            END		
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;
        THROW;
    END CATCH

    
RETURN @fileSystemNodeId
