﻿CREATE VIEW dbo.FolderNodeView
    AS SELECT dbo.FileSystemNode.ParentNodeId, 
              dbo.FileSystemNode.ParentNodePath, 
              dbo.FileSystemNode.NodeType,
              dbo.FolderNode.FileSystemNodeId,
              dbo.FolderNode.FolderNodeName
    FROM FileSystemNode LEFT JOIN dbo.FolderNode ON dbo.FolderNode.FileSystemNodeId = dbo.FileSystemNode.Id;
