﻿CREATE VIEW dbo.BinaryNodeView
    AS SELECT dbo.FileSystemNode.ParentNodeId, 
              dbo.FileSystemNode.ParentNodePath, 
              dbo.FileSystemNode.NodeType,
              dbo.BinaryNode.FileSystemNodeId,
              dbo.BinaryNode.BinaryNodeName
    FROM FileSystemNode LEFT JOIN [dbo].BinaryNode ON dbo.BinaryNode.FileSystemNodeId = dbo.FileSystemNode.Id;
