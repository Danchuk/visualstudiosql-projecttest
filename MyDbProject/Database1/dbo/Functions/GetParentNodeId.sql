﻿CREATE FUNCTION [dbo].[GetParentNodeId]
(
	@parentNodePath NVARCHAR (MAX)
)
RETURNS INT
AS
BEGIN
	DECLARE @parentNodeId     INT

    SET @parentNodeId = (SELECT dbo.FolderNode.Id FROM dbo.FolderNode 
            RIGHT JOIN dbo.FileSystemNode ON dbo.FolderNode.FileSystemNodeId = dbo.FileSystemNode.Id 
            WHERE CONCAT(dbo.FileSystemNode.ParentNodePath, dbo.FolderNode.FolderNodeName, '/') = @parentNodePath);

    IF @parentNodeId IS NULL
        SET @parentNodeId = -1;

	RETURN @parentNodeId
END
