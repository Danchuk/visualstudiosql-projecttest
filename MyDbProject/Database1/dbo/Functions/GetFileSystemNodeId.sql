﻿CREATE FUNCTION [dbo].[GetFileSystemNodeId]
(
    @parentNodePath NVARCHAR (MAX),
    @nodeName       NVARCHAR (500),
    @nodeType       int
)
RETURNS INT
AS
BEGIN
	DECLARE @fileSystemNodeId      INT

    SET @fileSystemNodeId = -1;

    IF (@nodeType = 1)
        SET @fileSystemNodeId = (SELECT dbo.FolderNode.FileSystemNodeId 
                                 FROM dbo.FolderNode 
                                 LEFT JOIN dbo.FileSystemNode ON dbo.FileSystemNode.Id = dbo.FolderNode.FileSystemNodeId 
                                 WHERE dbo.FileSystemNode.ParentNodePath = @parentNodePath AND dbo.FolderNode.FolderNodeName = @nodeName);
    ELSE IF (@nodeType = 2)
        SET @fileSystemNodeId = (SELECT dbo.BinaryNode.FileSystemNodeId 
                                 FROM dbo.BinaryNode 
                                 LEFT JOIN dbo.FileSystemNode ON dbo.FileSystemNode.Id = dbo.BinaryNode.FileSystemNodeId 
                                 WHERE dbo.FileSystemNode.ParentNodePath = @parentNodePath AND dbo.BinaryNode.BinaryNodeName = @nodeName);

    IF (@fileSystemNodeId IS NULL)
        SET @fileSystemNodeId = -1;

	RETURN @fileSystemNodeId
END
