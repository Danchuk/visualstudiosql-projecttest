﻿CREATE FUNCTION [dbo].[GetFolderNodeId]
(
	@parentNodePath NVARCHAR (MAX),
    @nodeName       NVARCHAR (500)
)
RETURNS INT
AS
BEGIN
	DECLARE @result INT

    SET @result = (SELECT dbo.FolderNode.Id 
                       FROM dbo.FolderNode 
                       LEFT JOIN dbo.FileSystemNode ON dbo.FileSystemNode.Id = dbo.FolderNode.FileSystemNodeId 
                       WHERE dbo.FileSystemNode.ParentNodePath = @parentNodePath AND dbo.FolderNode.FolderNodeName = @nodeName);
    IF @result IS NULL
        SET @result = -1; 
	RETURN @result
END
