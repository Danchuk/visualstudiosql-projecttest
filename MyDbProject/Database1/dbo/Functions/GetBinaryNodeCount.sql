﻿CREATE FUNCTION [dbo].[GetBinaryNodeCount]
(
	@fileSystemNodeId int
)
RETURNS INT
AS
BEGIN
	RETURN (SELECT COUNT(*) FROM dbo.BinaryNode WHERE dbo.BinaryNode.FileSystemNodeId = @fileSystemNodeId);
END
