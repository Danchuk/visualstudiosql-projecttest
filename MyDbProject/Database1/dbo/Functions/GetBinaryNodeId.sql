﻿CREATE FUNCTION [dbo].[GetBinaryNodeId]
(
	@parentNodePath NVARCHAR (MAX),
    @nodeName       NVARCHAR (500)
)
RETURNS INT
AS
BEGIN
	DECLARE @result INT

    SET @result = (SELECT dbo.BinaryNode.Id 
                       FROM dbo.BinaryNode 
                       LEFT JOIN dbo.FileSystemNode ON dbo.FileSystemNode.Id = dbo.BinaryNode.FileSystemNodeId 
                       WHERE dbo.FileSystemNode.ParentNodePath = @parentNodePath AND dbo.BinaryNode.BinaryNodeName = @nodeName);

    IF @result IS NULL
        SET @result = -1;

	RETURN @result
END
