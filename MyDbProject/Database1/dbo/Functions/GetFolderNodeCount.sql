﻿CREATE FUNCTION [dbo].[GetFolderNodeCount]
(
	@fileSystemNodeId int
)
RETURNS INT
AS
BEGIN
	RETURN (SELECT COUNT(*) FROM dbo.FolderNode WHERE dbo.FolderNode.FileSystemNodeId = @fileSystemNodeId);
END
